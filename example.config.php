<?php
// HTTP
define('HTTP_SERVER', 'http://opencart.local/');

// HTTPS
define('HTTPS_SERVER', 'http://opencart.local/');

// DIR
define('DIR_APPLICATION', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/catalog/');
define('DIR_SYSTEM', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/');
define('DIR_IMAGE', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/image/');
define('DIR_LANGUAGE', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/catalog/language/');
define('DIR_TEMPLATE', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/catalog/view/theme/');
define('DIR_CONFIG', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/config/');
define('DIR_CACHE', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/storage/download/');
define('DIR_LOGS', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp562/htdocs/iWorkshop/opencart-demo/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencart-demo');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
