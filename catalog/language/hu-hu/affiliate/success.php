<?php
$_['heading_title']			='Partner fiókja elkészült!';
$_['text_message']			='<p>Gratulálunk! Új fiókja sikeresen elkészült.</p> <p>Ön most már tagja a/az %s 
partnerségnek.</p> <p>Ha bármilyen kérdése merülne fel a Partner rendszer kezelésével kapcsolatban, kérjük érdeklődjön e-mailben a webáruház tulajdonosánál.</p> <p>Visszaigazoló e-mailt küldtünk e-mail fiókjába. Ha nem érkezik meg egy órán belül, kérjük<a href="%s">lépjen kapcsolatva velünk</a>!</p>';
$_['text_approval']			='<p>Köszönjük, hogy a(z) %s partner programjába regisztrált!</p><p>A fiókja jóváhagyásáról e-mail-ben értesíti a web áruház üzemeltetője.</p><p>Ha bármilyen kérdése lenne a partner program mûködésével kapcsolatban, lépjen kapcsolatba <a href="%s">velünk</a>.</p>';
$_['text_account']			='Fiók';
$_['text_success']			='Sikeres regisztráció';
?>