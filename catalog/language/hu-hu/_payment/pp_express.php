<?php
$_['express_text_title']			='Rendelés megerősítése';
$_['text_title']			='PayPal Express (Magában foglalja a hitel és bankkártyákat)';
$_['button_continue']			='Folytatás';
$_['text_cart']			='Kosár';
$_['text_shipping_updated']			='Szállítási állapot frissítve';
$_['text_trial']			='%s every %s %s for %s payments then ';
$_['text_recurring']			='%s minden %s %s';
$_['text_recurring_item']			='előfizetés';
$_['text_length']			=' for %s payments';
$_['express_entry_coupon']			='Kérem, írja be kuponkódját:';
$_['button_express_coupon']			='Hozzáad';
$_['button_express_confirm']			='Megerősít';
$_['button_express_login']			='Tovább a PayPal-ra';
$_['button_express_shipping']			='Szállítás frissítése';
$_['button_cancel_recurring']			='Befizetés visszavonása';
$_['error_heading_title']			='Hiba történt';
$_['error_too_many_failures']			='A befizetés túl sokszor volt sikertelen';
?>