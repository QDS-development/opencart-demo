<?php
$_['text_title']			='Hitelkártya / Bankkártya (SagePay)';
$_['text_credit_card']			='Kártya adatai';
$_['text_card_type']			='Kártya típus: ';
$_['text_card_name']			='Kártya neve: ';
$_['text_card_digits']			='Utolsó számok: ';
$_['text_card_expiry']			='Lejárat: ';
$_['text_trial']			='%s every %s %s for %s payments then ';
$_['text_recurring']			='%s minden %s %s';
$_['text_length']			=' for %s payments';
$_['entry_card']			='Új vagy meglévő kártya: ';
$_['entry_card_existing']			='Meglévő';
$_['entry_card_new']			='Új';
$_['entry_card_save']			='Jegyezze meg a kártya adatait';
$_['entry_cc_owner']			='Kártyatulajdonos';
$_['entry_cc_type']			='Kártya típusa';
$_['entry_cc_number']			='Kártyaszám';
$_['entry_cc_start_date']			='A kártya aktiválásának ideje';
$_['entry_cc_expire_date']			='A kártya lejárati ideje';
$_['entry_cc_cvv2']			='Biztonsági kód (CVV2)';
$_['entry_cc_issue']			='Kártyát kibocsájtó bank neve';
$_['entry_cc_choice']			='Válasszon a meglévő kártyák közül';
$_['help_start_date']			='(ha elérhető)';
$_['help_issue']			='(csak Maestro és Solo kártyák esetén)';
?>