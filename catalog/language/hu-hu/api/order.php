<?php
$_['text_success']			='A rendelés módosítása sikeresen megtörtént';
$_['error_permission']			='Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_customer']			='A megrendelő adatait be kell állítani!';
$_['error_payment_address']			='Számlázási cím megadása kötelező!';
$_['error_payment_method']			='Fizetési mód megadása kötelező!';
$_['error_no_payment']			='Figyelem: Nincs rendelkezésre álló fizetési mód!';
$_['error_shipping_address']			='Szállítási cím megadása kötelező!';
$_['error_shipping_method']			='Szállítási mód nincs megadva!';
$_['error_no_shipping']			= 'Figyelem: Nincs rendelkezésre álló szállítási mód!';
$_['error_stock']			='A *** jelölt termék(ek) nincsenek raktáron a kért mennyiségben!';
$_['error_minimum']			='Minimális rendelési összeg %s %s!';
$_['error_not_found']			='Figyelem: A megrendelés nem található!';
?>