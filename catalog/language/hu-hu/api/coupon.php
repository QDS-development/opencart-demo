<?php
$_['text_success']			='A kupon sikeresen felhasználva!';
$_['error_permission']			='Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_coupon']			='Figyelem: A kupon kódja hibás, lejárt vagy elérte a felhasználási limitet!';
?>