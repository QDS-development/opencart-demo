<?php
$_['heading_title']			='Kuponkód';
$_['text_coupon']			='Kupon (%s)';
$_['text_success']			='A kupon sikeresen felhasználva!';
$_['entry_coupon']			='Adja meg a kuponkódot';
$_['error_coupon']			='A kupon érvénytelen!';
$_['error_empty']			='Kérjük, írja be a kupon kódját!';
?>