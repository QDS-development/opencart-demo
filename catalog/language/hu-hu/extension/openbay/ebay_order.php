<?php
// Text
$_['text_total_shipping']		= 'Szállítás';
$_['text_total_discount']		= 'Engedmény';
$_['text_total_tax']			= 'Adó';
$_['text_total_sub']			= 'Részösszeg';
$_['text_total']				= 'Össesen';
$_['text_smp_id']				= 'Selling Manager sale ID: ';
$_['text_buyer']				= 'Vevő felhasználónév: ';