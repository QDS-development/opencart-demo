<?php
// Text
$_['text_paid_amazon'] 			= 'Paid on Amazon US';
$_['text_total_shipping'] 		= 'Szállítás';
$_['text_total_shipping_tax'] 	= 'Szállítási díj';
$_['text_total_giftwrap'] 		= 'Ajándék csomagolás';
$_['text_total_giftwrap_tax'] 	= 'Ajándék csomagolás díja';
$_['text_total_sub'] 			= 'Részösszeg';
$_['text_tax'] 					= 'Díj';
$_['text_total'] 				= 'Összesen';