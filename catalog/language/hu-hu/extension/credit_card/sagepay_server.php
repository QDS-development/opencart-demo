<?php
// Heading
$_['heading_title']         = 'Sagepay Server Kártyák';

// Text
$_['text_empty']		    = 'Nincsenek mentett kártyái';
$_['text_account']          = 'Fiók';
$_['text_card']			    = 'SagePay Server Kártya Kezelés';
$_['text_fail_card']	    = 'There was an issue removing your SagePay card, Please contact the shop administrator for help.';
$_['text_success_card']     = 'SagePay kártya sikeresen eltávolítva';
$_['text_success_add_card'] = 'SagePay kártya sikeresen hozzáadva';

// Column
$_['column_type']		    = 'Kártya típus';
$_['column_digits']	        = 'Utolsó számjegyek';
$_['column_expiry']		    = 'Lejárat';

// Entry
$_['entry_cc_owner']        = 'Kártya tulajdonos';
$_['entry_cc_type']         = 'Kártya típus';
$_['entry_cc_number']       = 'Kártya szám';
$_['entry_cc_expire_date']  = 'Kártya lejárati dátum';
$_['entry_cc_cvv2']         = 'Kártya biztonsági kód (CVV2)';
$_['entry_cc_choice']       = 'Válasszon egy kártyát';

// Button
$_['button_add_card']       = 'Kártya hozzáadása';
$_['button_new_card']       = 'Új Kártya hozzáadása';


