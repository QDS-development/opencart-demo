<?php
// Text
$_['text_captcha'] = 'Captcha';

// Entry
$_['entry_captcha'] = 'Írja a kódot az alábbi mezőbe!';

// Error
$_['error_captcha'] = 'Az ellenőrző kód nem egyezik a képpel!';
