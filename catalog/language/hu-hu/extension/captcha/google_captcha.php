<?php
// Text
$_['text_captcha'] = 'Captcha';

// Entry
$_['entry_captcha'] = 'Kérjük töltse ki az alábbi captcha ellenőrzést!';

// Error
$_['error_captcha'] = 'A Captcha feladatra adott válasz helytelen volt!';
