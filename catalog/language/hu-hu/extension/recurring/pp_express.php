<?php
// Text
$_['text_title']          = 'PayPal Express Pénztár';
$_['text_canceled']       = 'Sikeresen visszavonta ezt a befizetést!';

// Button
$_['button_cancel']       = 'Cancel Recurring Payment';

// Error
$_['error_not_cancelled'] = 'Hiba: %s';
$_['error_not_found']     = 'Could not cancel recurring profile';