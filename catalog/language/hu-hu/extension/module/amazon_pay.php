<?php
// Heading
$_['heading_title'] = 'Amazon Fizetés';

// Text
$_['text_module'] = 'Modulok';
$_['text_success'] = 'Sikeresen módosította a Amazon Fizetés modult!';
$_['text_content_top'] = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left'] = 'Column Left';
$_['text_column_right'] = 'Column Right';
$_['text_pwa_button'] = 'Pay with Amazon';
$_['text_pay_button'] = 'Pay';
$_['text_a_button'] = 'A';
$_['text_gold_button'] = 'Gold';
$_['text_darkgray_button'] = 'Dark Gray';
$_['text_lightgray_button'] = 'Light Gray';
$_['text_small_button'] = 'Small';
$_['text_medium_button'] = 'Medium';
$_['text_large_button'] = 'Large';
$_['text_x_large_button'] = 'X-Large';

//Entry
$_['entry_button_type'] = 'Button Type';
$_['entry_button_colour'] = 'Button Colour';
$_['entry_button_size'] = 'Button Size';
$_['entry_layout'] = 'Layout';
$_['entry_position'] = 'Position';
$_['entry_status'] = 'Status';
$_['entry_sort_order'] = 'Sort Order';

//Error
$_['error_permission'] = 'Figyelem: Nincs jogosultsága a Amazon Fizetés modul módosításához!';