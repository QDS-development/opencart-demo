<?php
// Heading
$_['heading_title']    = 'Partner Program';

// Text
$_['text_register']    = 'Regisztrál';
$_['text_login']       = 'Bejelentkezés';
$_['text_logout']      = 'Kijelentkezés';
$_['text_forgotten']   = 'Elfelejtett jelszó';
$_['text_account']     = 'Fiókom';
$_['text_edit']        = 'Fiók szerkesztése';
$_['text_password']    = 'Jelszó';
$_['text_payment']     = 'Fizetési módok';
$_['text_tracking']    = 'Partner nyomonkövetés';
$_['text_transaction'] = 'Tranzakciók';
