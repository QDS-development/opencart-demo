<?php
// Heading
$_['heading_title']				   = 'Klarna Checkout';
$_['heading_title_success']		   = 'Klarna Checkout rendelése sikeresen feladva!';

// Text
$_['text_title']				   = 'Klarna Checkout';
$_['text_basket']				   = 'Kosár';
$_['text_checkout']				   = 'Pénztár';
$_['text_success']				   = 'Siker';
$_['text_choose_shipping_method']  = 'Válasszon szállítási módot';
$_['text_sales_tax']			   = 'Forgalmi adó';