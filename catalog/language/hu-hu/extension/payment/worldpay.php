<?php
$_['text_title']			='Hitelkártya / Bankkártya (WorldPay)';
$_['text_credit_card']			='Kártya részletek';
$_['text_card_type']			='Kártya típus: ';
$_['text_card_name']			='Kártyán szereplő név: ';
$_['text_card_digits']			='Utolsó számjegyek: ';
$_['text_card_expiry']			='Lejárat: ';
$_['text_trial']			='%s every %s %s for %s payments then ';
$_['text_recurring']			='%s every %s %s';
$_['text_length']			=' for %s payments';
$_['text_confirm_delete']			='Biztos benne, hogy törölni szeretné ezt a kártyát?';
$_['text_card_success']			='Kártya sikeresen eltávolítva.';
$_['text_card_error']			='Hiba történt a kártya eltávolításakor. Segítségért kérjük, lépjen kapcsolatba az üzlet
adminisztrátorával';
$_['entry_card']			='Új vagy Meglévő kártya: ';
$_['entry_card_existing']			='Meglévő';
$_['entry_card_new']			='Új';
$_['entry_card_save']			='Remember Card Details';
$_['entry_cc_cvc']			='Kártya biztonsági kód (CVC)';
$_['entry_cc_choice']			='Válasszon egy meglévő kártyát';
$_['button_delete_card']			='Kártya törlése';
$_['error_process_order']			='Hiba történt a megrendelés feldolgozásakor. Segítségért kérjük, lépjen 
kapcsolatba az üzlet adminisztrátorával.';
?>