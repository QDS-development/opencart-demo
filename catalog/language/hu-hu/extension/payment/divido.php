<?php
$_['text_checkout_title']      = 'Részletfizetés';
$_['text_choose_plan']         = 'Válassza ki a tervezetet';
$_['text_choose_deposit']      = 'Válassza ki az előleget';
$_['text_monthly_payments']    = 'havi fizetés';
$_['text_months']              = 'hónapok';
$_['text_term']                = 'Időtartam';
$_['text_deposit']             = 'Előleg';
$_['text_credit_amount']       = 'Hitelköltség';
$_['text_amount_payable']      = 'Összes fizetendő';
$_['text_total_interest']      = 'Total interest APR';
$_['text_monthly_installment'] = 'Havi részletfizetés';
$_['text_redirection']         = 'A megrendelés jóváhagyásakor a tranzakció befejezéséhez átirányítjuk a Divido oldalára';
$_['divido_checkout']          = 'Jóváhagyás és fizetés Dividóval';
$_['deposit_to_low']           = 'Az előleg összege túl alacsony';
$_['credit_amount_to_low']     = 'A hitel összege túl alacsony';
$_['no_country']               = 'Az ország nem elfogadott';
