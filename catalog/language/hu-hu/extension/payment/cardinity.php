<?php
// Text
$_['text_title']						= 'Hitelkártya / Bankkártya (Cardinity)';
$_['text_payment_success']				= 'Sikeres befizetés. Részletek alább találhatók';
$_['text_payment_failed']				= 'Sikertelen befizetés. Részletek alább találhatók';

// Entry
$_['entry_holder']						= 'Kártyatulajdonos neve';
$_['entry_pan']							= 'Kártyaszám';
$_['entry_expires']						= 'Lejárat';
$_['entry_exp_month']					= 'Hónap';
$_['entry_exp_year']					= 'Év';
$_['entry_cvc']							= 'CVC';

// Error
$_['error_process_order']				= 'Hiba a rendelés feldolgozásában. Kérjük, lépjen kapcsolatba az üzlet 
adminisztrátorával.';
$_['error_invalid_currency']			= 'Kérjük, használjon valós pénznemet.';
$_['error_finalizing_payment']			= 'Error finalizing payment.';
$_['error_unknown_order_id']			= 'Ezzel a megrendelés azonosítóval nem található cardinity befizetés.';
$_['error_invalid_hash']				= 'Invalid hash.';
$_['error_payment_declined']			= 'A kibocsátó bank csökkentette a befizetést.';

// Button
$_['button_confirm']					= 'Befizetés';