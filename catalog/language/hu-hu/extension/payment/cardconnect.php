<?php
// Text
$_['text_title']            = 'Hitelkártya / Bankkártya';
$_['text_card_details']     = 'Kártya részletek';
$_['text_echeck_details']   = 'eCheck részletek';
$_['text_card']             = 'Kártya';
$_['text_echeck']           = 'eCheck';
$_['text_wait']             = 'Kérjük, várjon!';
$_['text_confirm_delete']   = 'Biztos benne, hogy törölni akarja a kártyát?';
$_['text_no_cards']         = 'Még nincs mentett kártyája';
$_['text_select_card']      = 'Kérjük, válasszon egy kártyát';

// Entry
$_['entry_method']          = 'Mód';
$_['entry_card_new_or_old'] = 'Új / Meglévő kártya';
$_['entry_card_new']        = 'Új';
$_['entry_card_old']        = 'Meglévő';
$_['entry_card_type']       = 'Kártya típus';
$_['entry_card_number']     = 'Kártya szám';
$_['entry_card_expiry']     = 'Lejárat';
$_['entry_card_cvv2']       = 'CVV2';
$_['entry_card_save']       = 'Kártya mentése';
$_['entry_card_choice']     = 'Válassza ki kártyáját';
$_['entry_account_number']  = 'Fiók szám';
$_['entry_routing_number']  = 'Routing Number';

// Button
$_['button_confirm']        = 'Rendelés jóváhagyása';
$_['button_delete']         = 'Kiválasztott kártya törlése';

// Error
$_['error_card_number']     = 'A Kártya szám minimum 1 maximum 19 karakter lehet!';
$_['error_card_type']       = 'Ez a Kártya típus nem érvényes!';
$_['error_card_cvv2']       = 'CVV2 minimum 1 maximum 4 karakter lehet!';
$_['error_data_missing']    = 'Hiányzó adat!';
$_['error_not_logged_in']   = 'Nincs bejelentkezve!';
$_['error_no_order']        = 'No matching order!';
$_['error_no_post_data']    = 'No $_POST data';
$_['error_select_card']     = 'Kérjük, válasszon ki egy kártyát!';
$_['error_no_card']         = 'Kártya nem található!';
$_['error_no_echeck']       = 'eCheck nem támogatott!';
$_['error_account_number']  = 'Fiók szám minimum 1 maximum 19 karakter lehet!';
$_['error_routing_number']  = 'Routing Number minimum 1 maximum 19 karakter lehet!';
$_['error_not_enabled']     = 'A modul nem engedélyezett';