<?php
$_['heading_title']			='Bejelentkezés és fizetés Amazon-nal';
$_['heading_address']			='Kérjük, válassza ki a szállítási címet';
$_['heading_payment']			='Kérjük, válassza ki a fizetési módot';
$_['heading_confirm']			='Rendelés összesítő';
$_['text_back']			='Vissza';
$_['text_cart']			='Kosár';
$_['text_confirm']			='Jóváhagy';
$_['text_continue']			='Folytatás';
$_['text_lpa']			='Bejelentkezés és fizetés Amazon-nal';
$_['text_enter_coupon']			='Ide írja be kuponkódját. Ha nincs, hagyja üresen a mezőt.';
$_['text_coupon']			='Kupon';
$_['text_tax_other']			='Díjak / Egyéb kezelési költségek';
$_['text_success_title']			='Rendelését sikeresen feladta!';
$_['text_payment_success']			='Rendelését sikeresen feladta. A megrendelés részletei alább láthatók';
$_['error_payment_method']			='Kérjük, válasszon ki egy fizetési módot';
$_['error_shipping']			='Kérjük, válasszon ki egy szállítási módot';
$_['error_shipping_address']			='Kérjük, válasszon ki egy szállítási címet';
$_['error_shipping_methods']			='There was an error retrieving your address from Amazon. Please contact the shop administrator for help.';
$_['error_no_shipping_methods']			='A kiválasztott címhez nincs szállítási opció. Kérjük, válasszon ki egy 
másik szállítási címet.';
$_['error_process_order']			='Hiba a rendelés feldolgozásában. Kérjük, lépjen kapcsolatba az üzlet 
adminisztrátorával.';
$_['error_login']			='Bejelentkezés sikertelen';
$_['error_login_email']			='Bejelentkezés sikertelen: %s fiók e-mailcím és az Amazon fiók e-mailcím nem egyezik';
$_['error_minimum']			='%s a minimum rendelési összeg az Amazon fizetési mód használatához!';
?>