<?php
// Heading
$_['heading_title']			= 'Válasszon fizetési tervezetet';

// Text
$_['text_title']			= 'PUT IT ON LAY-BUY powered by PayPal';
$_['text_plan_preview']		= 'Tervezet előnézet';
$_['text_payment']			= 'Befizetés';
$_['text_due_date']			= 'Esedékesség napja';
$_['text_amount']			= 'Összeg';
$_['text_downpayment']		= 'Előleg';
$_['text_today']			= 'Ma';
$_['text_delivery_msg']		= 'A vásárolt termék/szolgáltatás kiszállításra kerül, amit az utolsó befizetés is 
megérkezett.';
$_['text_fee_msg']			= '0.9%-os adminisztrációs díj a Lay-Buy.com részére fizetendő.';
$_['text_month']			= 'Hónap';
$_['text_months']			= 'Hónapok';
$_['text_status_1']			= 'Függőben lévő';
$_['text_status_5']			= 'Befejezett';
$_['text_status_7']			= 'Visszavont';
$_['text_status_50']		= 'Felülvizsgálatra kérve';
$_['text_status_51']		= 'Javított';
$_['text_comment']			= 'Updated by Lay-Buy';

// Entry
$_['entry_initial']			= 'Initial Payment';
$_['entry_months']			= 'Hónapok';

// Button
$_['button_confirm']		= 'Rendelés jóváhagyása';