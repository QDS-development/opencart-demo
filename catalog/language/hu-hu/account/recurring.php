<?php
$_['heading_title']			='Előfizetéseim';


$_['text_account']			='Fiók';
$_['text_recurring']			='Recurring payment Information';
$_['text_recurring_detail']			='Előfizetés beállításai';
$_['text_order_recurring_id'] = 'Előfizetés ID:';
$_['text_date_added']			='Létrehozva: ';
$_['text_status']			='Állapot:';
$_['text_payment_method']			='Fizetési mód: ';
$_['text_order_id']           = 'Megrendelés ID:';
$_['text_product']			='Termék: ';
$_['text_quantity']			='Mennyiség: ';
$_['text_description']        = 'Leírás';
$_['text_reference']          = 'Referencia';
$_['text_transactions']			='Tranzakcióim';


$_['text_status_1']           = 'Aktív';
$_['text_status_2']           = 'Inaktív';
$_['text_status_3']           = 'Törölt';
$_['text_status_4']           = 'Felfüggesztett';
$_['text_status_5']           = 'Eljárt';
$_['text_status_6']           = 'Függő';


$_['text_transaction_date_added']			='Létrehozás dátuma';
$_['text_transaction_payment']			='Fizetés';
$_['text_transaction_outstanding_payment']			='Kiemelkedő befizetés';
$_['text_transaction_skipped']			='Kihagyott befizetés';
$_['text_transaction_failed']			='A befizetés nem sikerült';
$_['text_transaction_cancelled']			='Törölve';
$_['text_transaction_suspended']			='Felfüggesztett';
$_['text_transaction_suspended_failed']			='Sikertelen fizetés miatt felfüggesztett';
$_['text_transaction_outstanding_failed']			='A befizetés nem sikerült';
$_['text_transaction_expired']			='Lejárt';


$_['text_empty']			='Nincsenek előfizetések';
$_['text_error']                 = 'A keresett előfizetés nem található!';
$_['text_cancelled']			='Előfizetés törölve';

$_['column_date_added']			='Létrehozás dátuma';
$_['column_type']			='Típus';
$_['column_amount']			='Összeg';
$_['column_status']			='Állapot';
$_['column_product']			='Termék';
$_['column_order_recurring_id']			='Profil azonosító';

$_['error_not_cancelled']			='Hiba: %s';
$_['error_not_found']			='Nem sikerült törölni az előfizetést';

$_['button_return']			='Visszaküldés';



//$_['column_action']			='Művelet';
//$_['button_continue']			='Tovább';
//$_['button_view']			='View';
//$_['text_order']			='Order: ';
//$_['text_action']			='Művelet';
//$_['text_empty_transactions']			='Az előfizetési profilhoz nem tartoznak tranzakciók';
//$_['text_recurring_id']			='Profil azonosító: ';
//$_['text_recurring_description']			='Leírás: ';
//$_['text_ref']			='Hivatkozás: ';
//$_['text_status_active']			='Aktív';
//$_['text_status_inactive']			='Inaktív';
//$_['text_status_cancelled']			='Törölve';
//$_['text_status_suspended']			='Felfüggesztett';
//$_['text_status_expired']			='Lejárt';
//$_['text_status_pending']			='Függőben';


?>