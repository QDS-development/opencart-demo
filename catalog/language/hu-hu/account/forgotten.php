<?php
$_['heading_title']			='Elfelejtette a jelszavát?';
$_['text_account']			='Fiók';
$_['text_forgotten']			='Elfelejtett jelszó';
$_['text_your_email']			='Az Ön e-mail címe';
$_['text_email']			='Adja meg a fiókjához tartozó e-mail címét. Majd kattintson a Tovább gombra és küldjük Önnek e-mailben az új jelszavát.';
$_['text_success']			='Az új jelszavát elküldtük az e-mail címére.';
$_['entry_email']			='E-mail cím';
$_['entry_password']  = 'Új jelszó';
$_['entry_confirm']   = 'Jóváhagy';
$_['error_email']			='A megadott e-mail cím nem szerepel adatbázisunkban! Kérjük, hogy próbálja meg újra!';
$_['error_approved']			='Figyelmeztetés: Első bejelentkezés előtt hagyja jóvá regisztrációját!';
$_['error_password']  = 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']   = 'A jelszó és a jelszó megerősítése nem egyezik!';
?>