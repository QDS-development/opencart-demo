<?php
// header
$_['heading_title']  = 'Jelszó reset';

// Text
$_['text_account']   = 'Fiók';
$_['text_password']  = 'Írja be az új jelszót!';
$_['text_success']   = 'Jelszavát sikeresen megváltoztatta';

// Entry
$_['entry_password'] = 'Jelszó';
$_['entry_confirm']  = 'Jóváhagyás';

// Error
$_['error_password'] = 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']  = 'A jelszó és a jelszó megerősítése nem egyezik!';
$_['error_code']     = 'A jelszó reset kód érvénytelen, vagy már korábban felhasználták!';