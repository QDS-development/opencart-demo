<?php
$_['heading_title']			='Letöltések';
$_['text_account']			='Fiók';
$_['text_downloads']			='Letöltéseim';
$_['text_empty']			='Önnek jelenleg nincs megrendelt letölthető terméke!';
$_['column_order_id']			='Rendelési azonosító';
$_['column_name']			='Név';
$_['column_size']			='Méret';
$_['column_date_added']			='Hozzáadás dátuma';
?>