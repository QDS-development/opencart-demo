<?php
$_['heading_title']			='Hűségpontok';
$_['column_date_added']			='Hozzáadva';
$_['column_description']			='Leírás';
$_['column_points']			='Pontok';
$_['text_account']			='Fiók';
$_['text_reward']			='Hűségpontjaim';
$_['text_total']			='Hűségpontjainak száma:';
$_['text_empty']			='Önnek nincs jelenleg hűségpontja!';
?>