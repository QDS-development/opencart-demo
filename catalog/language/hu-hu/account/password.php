<?php
$_['heading_title']			='Jelszó megváltoztatása';
$_['text_account']			='Fiók';
$_['text_password']			='Jelszó';
$_['text_success']			='A jelszavát sikeresen módosította.';
$_['entry_password']			='Jelszó';
$_['entry_confirm']			='Jelszó mégegyszer';
$_['error_password']			='A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']			='A jelszó megerősítése sikertelen!';
?>