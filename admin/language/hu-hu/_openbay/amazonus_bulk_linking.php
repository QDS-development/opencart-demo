<?php
$_['heading_title']			='Bulk Linking';
$_['text_openbay']			='OpenBay Pro';
$_['text_amazon']			='Amazon';
$_['button_load']			='Load';
$_['button_link']			='Link ';
$_['text_local']			='Local';
$_['text_load_listings']			='Load your listings from Amazon';
$_['text_report_requested']			='Successfully requested Listing Report from Amazon';
$_['text_report_request_failed']			='Could not request Listing Report';
$_['text_loading']			='Loading items';
$_['column_asin']			='ASIN';
$_['column_price']			='Ár';
$_['column_name']			='Név';
$_['column_sku']			='SKU';
$_['column_quantity']			='Quantity';
$_['column_combination']			='Combination';
$_['error_bulk_link_permission']			='Bulk linking is not available on your plan, please upgrade to use this feature.';
?>