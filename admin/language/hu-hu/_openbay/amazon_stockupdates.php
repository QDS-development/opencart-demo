<?php
$_['heading_title']			='Stock updates';
$_['text_openbay']			='OpenBay Pro';
$_['text_amazon']			='Amazon EU';
$_['text_empty']			='No results!';
$_['entry_date_start']			='Kezdő dátum';
$_['entry_date_end']			='Befejezés dátum';
$_['column_ref']			='Ref';
$_['column_date_requested']			='Date requested';
$_['column_date_updated']			='Date updated';
$_['column_status']			='Állapot';
$_['column_sku']			='Amazon SKU';
$_['column_stock']			='Stock';
?>