<?php
$_['heading_title']			='Szűrők';
$_['text_success']			='Sikeresen módosított szűrők!';
$_['text_list']			='Szűrő listák';
$_['text_add']			='Szűrő hozzáadása';
$_['text_edit']			='Szűrő szerkesztése';
$_['column_group']			='Szűrő csoport';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['entry_group']			='Szűrő csoport név:';
$_['entry_name']			='Szűrő név:';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Figyelem: Nincs jogosultsága a szűrő módosításához!';
$_['error_group']			='A Szűrő csoport neve minimum 1 és maximum 64 karakter lehet!';
$_['error_name']			='A Szűrő neve minimum 1 és maximum 64 karakter lehet!';
?>