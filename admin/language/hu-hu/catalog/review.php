<?php
$_['heading_title']			='Vélemények';
$_['text_success']			='A vélemény módosítása sikeresen megtörtént!';
$_['text_list']			='Értékelések';
$_['text_add']			='Értékelés hozzáadása';
$_['text_edit']			='Értékelés szerkesztése';
$_['column_product']			='Termék';
$_['column_author']			='Hozzászóló';
$_['column_rating']			='Értékelés';
$_['column_status']			='Állapot';
$_['column_date_added']			='Érkezett';
$_['column_action']			='Művelet';
$_['entry_product']			='Termék:';
$_['entry_author']			='Hozzászóló:';
$_['entry_rating']			='Értékelés:';
$_['entry_status']			='Állapot';
$_['entry_text']			='Szöveg:';
$_['entry_date_added']			='Hozzáadás dátuma';
$_['help_product']			='(automatikus kitöltés)';
$_['error_permission']			='Nincs jogosultsága a vélemények módosításához!';
$_['error_product']			='A termék megadása kötelező!';
$_['error_author']			='A hozzászóló neve legalább 3 és legfeljebb 64 karakterből álljon!';
$_['error_text']			='A vélemény legalább 1 karakterből álljon!';
$_['error_rating']			='Az értékelés megadása kötelező!';
?>