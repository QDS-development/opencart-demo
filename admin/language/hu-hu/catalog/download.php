<?php
$_['heading_title']			='Letöltéseim';
$_['text_success']			='A letöltés(ek) frissítése sikeresen megtörtént!';
$_['text_list']			='Letöltések listája';
$_['text_add']			='Letöltés hozzáadása';
$_['text_edit']			='Letöltés szerkesztése';
$_['text_upload']			='A fájl sikeresen feltöltve!';
$_['column_name']			='Letöltés neve';
$_['column_date_added']			='Hozzáadás dátuma';
$_['column_action']			='Művelet';
$_['entry_name']			='Letöltés neve:';
$_['entry_filename']			='Fájlnév';
$_['entry_mask']			='Maszk:';
$_['help_filename']			='Feltölthet a feltöltés gomb használatával, vagy FTP-n keresztül a download mappába. A 
feltöltés részleteit alább töltheti ki.';
$_['help_mask']			='It is recommended that the filename and the mask are different to stop people trying to directly link to your downloads.';
$_['error_permission']			='Nincs jogosultsága a letöltések módosításához!';
$_['error_name']			='A név legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_upload']			='Feltöltés szükséges!';
$_['error_filename']			='A fájlnév legalább 3, és legfeljebb 128 karakterből álljon!';
$_['error_exists']			='A fájl nem található!';
$_['error_mask']			='A maszk név legalább 3, és legfeljebb 128 karakterből álljon!';
$_['error_filetype']			='Érvénytelen fájltípus!';
$_['error_product']			='Ezt a letöltést nem törölheti, mert hozzárendelte a(z) %s termékhez!';
?>