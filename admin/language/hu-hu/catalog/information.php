<?php
$_['heading_title']			='Egyéb oldalak';
$_['text_success']			='Az oldal frissítése sikeresen megtörtént!';
$_['text_list']			='Egyéb oldalak listája';
$_['text_add']			='Egyéb oldal hozzáadása';
$_['text_edit']			='Egyéb oldal szerkesztése';
$_['text_default']			='Alapértelmezett';
$_['column_title']			='Oldal neve';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['entry_title']			='Oldal neve:';
$_['entry_description']			='Leírás:';
$_['entry_store']			='Webáruház';
$_['entry_meta_title']			='Meta cím';
$_['entry_meta_keyword']			='Meta kulcsszavak';
$_['entry_meta_description']			='Meta leírás';
$_['entry_keyword']			='SEO link';
$_['entry_bottom']			='Lábléc';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['entry_layout']			='Felület beállítás:';
$_['help_keyword']			='Ne használjon szóközt a szavak között, inkább kötőjelet vagy aláhúzást és győződjön meg róla, hogy ez a link még nem létezik ezen az oldalon.';
$_['help_bottom']			='Mutassa a láblécben.';
$_['error_warning']			='Figyelem: Kérlek ellenőrizd figyelmesen az űrlapot hibákért!';
$_['error_permission']			='Figyelem: Az információk módosítása az Ön számára nem engedélyezett!';
$_['error_title']			='Az információ címe legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_description']			='A leírás legalább 3 karakterből álljon!';
$_['error_meta_title']			='A Meta cím minimum 3 és maximum 255 karakter lehet!';
$_['error_keyword']			='A SEO kulcsszó már használatban van!';
$_['error_account']			='Figyelem: Ez az oldal nem törölhető, mert használatban van az alapértelmezett áruháznál.';
$_['error_checkout']			='Figyelem: Ez az oldal nem törölhető, mert használatban van az alapértelmezett áruház kijelentkezési folyamatánál.';
$_['error_affiliate']			='Figyelem: Ezt az egyéb oldalt nem lehet törölni, mert a bolt partner szerződési 
feltételének van kijelölve !';
$_['error_return']			='Figyelem: Ezt az egyéb oldalt nem lehet törölni, mert a bolt visszaküldési feltételének
 van kijelölve!';
$_['error_store']			='Figyelem: Ez az egyéb oldal nem törölhető, mert használatban van a %s áruházban.';
?>