<?php
$_['heading_title']			='Előfizetői profilok';
$_['text_success']			='Sikeresen módosítjotta az előfizetői profilokat';
$_['text_list']			='Előfizetői profilok listája';
$_['text_add']			='Előfizetői profil hozzáadáas';
$_['text_edit']			='Előfizetői profil szerkesztése';
$_['text_day']			='Leírás';
$_['text_week']			='hét';
$_['text_semi_month']			='félhónap';
$_['text_month']			='hónap';
$_['text_year']			='év';
$_['text_recurring']			='<p><i class="fa fa-info-circle"></i> Az előfizetői díjak a gyakoriság és a ciklikusság alapján vannak kiszámítva.</p><p>For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks.</p><p>The duration is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.</p>';
$_['text_profile']			='Előfizetői profilok';
$_['text_trial']			='Próba profil';
$_['entry_name']			='Név';
$_['entry_price']			='Ár';
$_['entry_duration']			='Időtartam';
$_['entry_cycle']			='Ciklus';
$_['entry_frequency']			='Gyakoriság';
$_['entry_trial_price']			='Próba ár';
$_['entry_trial_duration']			='Próba időtartam';
$_['entry_trial_status']			='Próba státusz';
$_['entry_trial_cycle']			='Próba ciklus';
$_['entry_trial_frequency']			='Próba gyakoriság';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['column_name']			='Név';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['error_warning']			='Figyelem: Kérjük javítsa ki a hibát az űrlapon!';
$_['error_permission']			='Figyelem: Nincs jogosultsága módosítani az előfizetői profilt';
$_['error_name']			='A profile név minimum 3 és maximum 255 karakter lehet!';
$_['error_product']			='Figyelem: Ez az előfizetői profil nem törölhető, mert jelenleg használatban van a(z) %s
 terméknél!';
?>