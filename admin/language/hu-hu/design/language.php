<?php
// Heading
$_['heading_title']    = 'Nyelvszerkesztő';

// Text
$_['text_success']     = 'Sikeresen módosítította a nyelvszerkesztőt!';
$_['text_edit']        = 'Fordítás szerkesztése';
$_['text_default']     = 'Alapértelmezett';
$_['text_store']       = 'Webáruház';
$_['text_language']    = 'Nyelv';
$_['text_translation'] = 'Válasszon ki egy fordítást';
$_['text_translation'] = 'Fordítások';

// Entry
$_['entry_key']        = 'Kulcs';
$_['entry_value']      = 'Érték';
$_['entry_default']    = 'Alapértelmezett';

// Error
$_['error_permission'] = 'Figyelem: Nincs jogosultsága szerkeszteni a nyelvszerkesztőt!';