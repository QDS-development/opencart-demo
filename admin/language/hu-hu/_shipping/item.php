<?php
$_['heading_title']			='Árucikkenkénti szállítási díj';
$_['text_shipping']			='Szállítás';
$_['text_success']			='Siker: Az árucikkenkénti szállítási díj módosítása megtörtént!';
$_['text_edit']			='Edit Per Item Shipping';
$_['entry_cost']			='Költség:';
$_['entry_tax_class']			='Tax Class';
$_['entry_geo_zone']			='Földrajzi zóna:';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Figyelmeztetés: Az árucikkenkénti szállítási díj módosítása az Ön számára nem engedélyezett!';
?>