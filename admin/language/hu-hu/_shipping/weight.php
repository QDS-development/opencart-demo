<?php
$_['heading_title']			='Súly';
$_['text_shipping']			='Szállítás';
$_['text_success']			='Siker: A súly szállítási mód módosítása megtörtént!';
$_['text_edit']			='Edit Weight Based Shipping';
$_['entry_rate']			='Rates';
$_['entry_tax_class']			='Tax Class';
$_['entry_geo_zone']			='Földrajzi zóna:';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['help_rate']			='Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
$_['error_permission']			='Figyelmeztetés: A súly szállítási mód módosítása az Ön számára nem engedélyezett!';
?>