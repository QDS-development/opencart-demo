<?php
$_['heading_title']			='Utánvétel';
$_['text_shipping']			='Szállítás';
$_['text_success']			='Az utánvétel szállítási modul módosítása sikeresen megtörtént!';
$_['text_edit']			='Edit Flat Rate Shipping';
$_['entry_cost']			='Költség:';
$_['entry_tax_class']			='Tax Class';
$_['entry_geo_zone']			='Földrajzi zóna:';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Az utánvétel szállítási modul módosításához nincs jogosultsága!';
?>