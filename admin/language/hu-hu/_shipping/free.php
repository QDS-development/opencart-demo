<?php
$_['heading_title']			='Ingyenes szállítás';
$_['text_shipping']			='Szállítás';
$_['text_success']			='Siker: Az ingyenes szállítási mód módosítása megtörtént!';
$_['text_edit']			='Edit Free Shipping';
$_['entry_total']			='Összesen';
$_['entry_geo_zone']			='Földrajzi zóna:';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['help_total']			='Sub-Total amount needed before the free shipping module becomes available.';
$_['error_permission']			='Figyelmeztetés: Az ingyenes szállítás módosítása az Ön számára nem engedélyezett!';
?>