<?php
$_['heading_title']			='Australia Post';
$_['text_shipping']			='Szállítás';
$_['text_success']			='Success: You have modified Australia Post shipping!';
$_['text_edit']			='Edit Australia Post Shipping';
$_['entry_postcode']			='Irányítószám';
$_['entry_express']			='Express Postage:';
$_['entry_standard']			='Standard Postage:';
$_['entry_display_time']			='Display Delivery Time';
$_['entry_weight_class']			='Weight Class';
$_['entry_tax_class']			='Tax Class:';
$_['entry_geo_zone']			='Geo Zone:';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['help_display_time']			='Do you want to display the shipping time? (e.g. Ships within 3 to 5 days)';
$_['help_weight_class']			='Set to grams.';
$_['error_permission']			='Warning: You do not have permission to modify Australia Post shipping!';
$_['error_postcode']			='Post Code must be 4 digits!';
?>