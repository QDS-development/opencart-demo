<?php
$_['text_approve_subject']			='%s - A partner fiókja aktív!';
$_['text_approve_welcome']			='Üdvözöljük, és köszönjük regisztrációját %s!';
$_['text_approve_login']			='Fiókja elkészült. Most már bejelentkezhet e-mail címével és jelszavával a következő helyen:';
$_['text_approve_services']			='Bejelentkezés után hozzáférhet előző rendeléseihez és szerkesztheti adatait.';
$_['text_approve_thanks']			='Köszönjük,';
$_['text_transaction_subject']			='%s - partneri jutalék';
$_['text_transaction_received']			='%s jutalék jóváírva!';
$_['text_transaction_total']			='Eddig összegyüjtött jutaléka: %s.';
?>