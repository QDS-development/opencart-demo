<?php
$_['text_subject']			='Ajándék kupont kaptott - %s';
$_['text_greeting']			='Gratulálok, Most kaptott egy ajándék kupont melynek értéke %s';
$_['text_from']			='Egy ajándék kupont küldött Önnek %s';
$_['text_message']			='Melyhez a következő üzenet társul:';
$_['text_redeem']			='Ahhoz hogy beváltssa a kupont, írja le a beváltáshoz szükséges kódot, ami <b>%s</b>, majd kattintson a linkre alul, hogy a vásárláshoz jusson. A kosárnál még a fizetés előtt tudja megadni a kódot.';
$_['text_footer']			='Ha kérdése van, tegye fel bátran.';
?>