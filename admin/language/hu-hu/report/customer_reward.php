<?php
$_['heading_title']			='Ügyfél hűségpontok';
$_['text_list']			='Customer Reward Points List';
$_['column_customer']			='Ügyfél neve';
$_['column_email']			='E-mail';
$_['column_customer_group']			='Vásárlói csoport';
$_['column_status']			='Állapot';
$_['column_points']			='Hűségpontjaim';
$_['column_orders']			='Rendelések száma';
$_['column_total']			='Összesen';
$_['column_action']			='Művelet';
$_['entry_date_start']			='Kezdő dátum';
$_['entry_date_end']			='Befejezés dátum';
$_['entry_customer']		= 'Customer';
?>