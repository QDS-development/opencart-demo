<?php
$_['heading_title']			='Megtekintett termékek';
$_['text_list']			='Megtekintett termékek listája';
$_['text_success']			='Törölted a megtekintett termékek Reportot!';
$_['column_name']			='Terméknév';
$_['column_model']			='Cikkszám';
$_['column_viewed']			='Megtekintések';
$_['column_percent']			='Százalékos arány';
$_['error_permission']			='Warning: You do not have permission to reset product viewed report!';
?>