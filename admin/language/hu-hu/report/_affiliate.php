<?php
$_['heading_title']			='Affiliate Commission Report';
$_['text_list']			='Affiliate Commission List';
$_['column_affiliate']			='Affiliate Name';
$_['column_email']			='E-mail';
$_['column_status']			='Állapot';
$_['column_commission']			='Commission';
$_['column_orders']			='Rendelések száma';
$_['column_total']			='Összesen';
$_['column_action']			='Művelet';
$_['entry_date_start']			='Kezdő dátum';
$_['entry_date_end']			='Befejezés dátum';
?>