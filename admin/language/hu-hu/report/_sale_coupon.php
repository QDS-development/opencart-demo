<?php
$_['heading_title']			='Kupon Report';
$_['text_list']			='Kuponok listája';
$_['column_name']			='Kupon neve';
$_['column_code']			='Kód';
$_['column_orders']			='Rendelések';
$_['column_total']			='Összesen';
$_['column_action']			='Művelet';
$_['entry_date_start']			='Kezdő dátum';
$_['entry_date_end']			='Befejezés dátum';
?>