<?php
$_['heading_title']			='Marketing Report';
$_['column_campaign']			='Kampány neve';
$_['column_code']			='Kód';
$_['column_clicks']			='Kattintások';
$_['column_orders']			='Rendelések száma';
$_['column_total']			='Összesen';
$_['entry_date_start']			='Kezdő dátum';
$_['entry_date_end']			='Befejezés dátum';
$_['entry_status']			='Rendelés státusza';
?>