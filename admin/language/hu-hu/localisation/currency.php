<?php
$_['heading_title']			='Pénznemek';
$_['text_success']			='Siker: A pénznem módosítása megtörtént!';
$_['text_list']			='Pénznemek listája';
$_['text_add']			='Pénznem hozzáadása';
$_['text_edit']			='Pénznem szerkesztése';
$_['text_iso']             = 'Az ISO pénznemek és beállítások listáját <a href="http://www.xe.com/iso4217.php" target="_blank">itt</a> találja.';
$_['column_title']			='Pénznem';
$_['column_code']			='Kód';
$_['column_value']			='Érték';
$_['column_date_modified']			='Frissítve';
$_['column_action']			='Művelet';
$_['entry_title']			='Pénznem megnevezése';
$_['entry_code']			='Kód';
$_['entry_value']			='Érték';
$_['entry_symbol_left']			='Rövidítés balra';
$_['entry_symbol_right']			='Rövidítés jobbra';
$_['entry_decimal_place']			='Tizedesjegyek';
$_['entry_status']			='Állapot';
$_['help_code']			='Ne változtassa meg, ha ez az alapértelmezett pénznem.';
$_['help_value']			='Állítsa 1.00000 -re, ha ez az alapértelmezett valuta.';
$_['error_permission']			='Figyelmeztetés: A pénznemek módosítása az Ön számára nem engedélyezett';
$_['error_title']			='A pénznem megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_code']			='A pénznem kódjának 3 karakterből kell állnia!';
$_['error_default']			='Figyelmeztetés: Ezt a pénznemet nem törölheti, mivel jelenleg ez az áruház alapértelmezésként hozzárendelt pénzneme!';
$_['error_store']			='Figyelmeztetés: Ez a valuta nem törölhető, mert a %s áruházhoz tartozik!';
$_['error_order']			='Figyelmeztetés: Ezt a pénznemet nem törölheti, mivel jelenleg %s rendeléshez hozzárendelt!';
?>