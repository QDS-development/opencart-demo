<?php
$_['heading_title']			='Rendelési státuszok';
$_['text_success']			='Siker: A rendelési státuszok módosítása megtörtént!';
$_['text_list']			='Rendelési státuszok listája';
$_['text_add']			='Rendelési státusz hozzáadása';
$_['text_edit']			='Rendelési státusz szerkesztése';
$_['column_name']			='Rendelési státusz';
$_['column_action']			='Művelet';
$_['entry_name']			='Rendelési státusz neve';
$_['error_permission']			='Figyelem: A rendelési státuszok módosítása az Ön számára nem engedélyezett';
$_['error_name']			='Figyelem: A rendelési státusz neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_default']			='Figyelem: Ezt a rendelési státusz nem törölheti, mert jelenleg hozzárendelt az üzlet alapértelmezett rendelési állapotához!';
$_['error_download']			='Figyelem: Ez rendelési státusz nem törölhető, mert ez az alapértelmezett a letöltésekhez!';
$_['error_store']			='Figyelem: Ez a rendelési státusz nem törölhető, mert ez az alapértelmezett a következő áruházban: %s!';
$_['error_order']			='Figyelem: Ezt a rendelési státusz nem törölheti, mert jelenleg %s rendeléshez hozzárendelt!';
?>