<?php
$_['heading_title']			='Adómértékek';
$_['text_success']			='Sikeresen módosított adómérték!';
$_['text_list']			='Adómértékek listája';
$_['text_add']			='Adómérték hozzáadása';
$_['text_edit']			='Adómérték szerkesztése';
$_['text_percent']			='Százalék';
$_['text_amount']			='Fix összeg';
$_['column_name']			='Adó neve';
$_['column_rate']			='Adó mértéke';
$_['column_type']			='Típus';
$_['column_geo_zone']			='Geo zóna';
$_['column_date_added']			='Hozzáadás dátuma';
$_['column_date_modified']			='Módosítás dátuma';
$_['column_action']			='Művelet';
$_['entry_name']			='Adó neve';
$_['entry_rate']			='Adó mértéke';
$_['entry_type']			='Típus';
$_['entry_customer_group']			='Vásárlói csoport';
$_['entry_geo_zone']			='Geo zóna';
$_['error_permission']			='Figyelem: Nincs jogosultsága módosítani az adóosztályt!';
$_['error_tax_rule']			='Figyelem: Ezt az adómértéket nem törölheti, mert használatban van a(z) %s adó 
osztályban!';
$_['error_name']			='Az adó neve minimum 3 és maximum 32 karakter lehet!';
$_['error_rate']			='Adómérték kötelező!';
?>