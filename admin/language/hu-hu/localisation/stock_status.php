<?php
$_['heading_title']			='Készletállapotok';
$_['text_success']			='Siker: A készletállapotok módosítása sikerült!';
$_['text_list']			='Készletállapotok listája';
$_['text_add']			='Készletállapotok hozzáadása';
$_['text_edit']			='Készletállapotok szerkesztése';
$_['column_name']			='Készletállapot neve';
$_['column_action']			='Művelet';
$_['entry_name']			='Készletállapot neve:';
$_['error_permission']			='Figyelmeztetés: A készletállapotok módosítása az Ön számára nem engedélyezett!';
$_['error_name']			='A készletállapot nevének hosszabbnak kell lennie, mint 3 és rövidebbnek, mint 32 karakter!';
$_['error_product']			='Figyelmeztetés: Ez a készletállapot nem törölhető, mivel korábban hozzárendelésre került a következő termékekhez: %s!';
?>