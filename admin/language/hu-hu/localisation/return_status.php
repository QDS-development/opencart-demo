<?php
$_['heading_title']			='Visszaküldések állapota';
$_['text_success']			='Sikeresen módosítottad a visszaküldések állapotát!';
$_['text_list']			='Visszaküldések listája';
$_['text_add']			='Visszaküldés hozzáadása';
$_['text_edit']			='Visszaküldés szerkesztése';
$_['column_name']			='Visszaküldés állapot neve';
$_['column_action']			='Művelet';
$_['entry_name']			='Visszaküldés állapot neve';
$_['error_permission']			='Figyelem: Nincs jogosultságod a Visszaküldés állapotok szerkesztéséhez!';
$_['error_name']			='Visszaküldés állapot neve legalább 3 és legfeljebb 32 karakter lehet!';
$_['error_default']			='Figyelem: Ezt a visszaküldés műveletet nem lehet törölni mivel alapértelmezett státusznak van megjelölve!';
$_['error_return']			='Figyelem: Ezt a visszaküldés műveletet nem lehet törölni mivel %s  visszaküldéshez van rendelve!';
?>