<?php
$_['heading_title']			='Zónák';
$_['text_success']			='Siker: A zóna frissítése megtörtént!';
$_['text_list']			='Zone List';
$_['text_add']			='Add Zone';
$_['text_edit']			='Edit Zone';
$_['column_name']			='Zóna';
$_['column_code']			='Zónakód';
$_['column_country']			='Ország';
$_['column_action']			='Művelet';
$_['entry_name']			='Zóna neve:';
$_['entry_code']			='Zónakód:';
$_['entry_country']			='Ország';
$_['entry_status']			='Állapot';
$_['error_permission']			='Figyelem: A zónák módosítása az Ön számára nem engedélyezett';
$_['error_name']			='Figyelem: A zóna neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_default']			='Figyelem: Ezt a zónát nem törölheti, mert jelenleg az üzlet alapértelmezett zónájaként hozzárendelt!';
$_['error_store']			='Figyelem:  Ez a zóna nem törölhető, mert ez az alapértelmezett a következő áruházban: %s!';
$_['error_address']			='Figyelem: Ezt a zónát nem törölheti, mert hozzá van rendelve a(z) %s címjegyzék bejegyzéshez!';
$_['error_affiliate']			='Figyelem: Ezt a visszaküldés műveletet nem lehet törölni mivel %s partner programhoz van rendelve!';
$_['error_zone_to_geo_zone']			='Figyelem: Ezt a zónát nem törölheti, mert jelenleg %s zóna hozzárendelt földrajzi zónákhoz!';
?>