<?php
$_['heading_title']			='Geo zónák';
$_['text_success']			='Siker: A földrajzi zóna frissítése megtörtént!';
$_['text_list']			='Geo zónak listája';
$_['text_add']			='Geo zóna hozzáadása';
$_['text_edit']			='Geo zóna szerkesztése';
$_['column_name']			='Földrajzi zóna';
$_['column_description']			='Leírás';
$_['column_action']			='Művelet';
$_['entry_name']			='Földrajzi zóna neve';
$_['entry_description']			='Leírás';
$_['entry_country']			='Ország';
$_['entry_zone']			='Zóna';
$_['error_permission']			='Figyelmeztetés: A földrajzi zónák módosítása az Ön számára nem engedélyezett';
$_['error_name']			='A földrajzi zóna neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_description']			='A leírás legalább 3, és legfeljebb 255 karakterből álljon!';
$_['error_tax_rate']			='Figyelmeztetés: Ezt a földrajzi zónát nem törölheti, mert jelenleg hozzárendelt egy vagy több áfakulcshoz!';
?>