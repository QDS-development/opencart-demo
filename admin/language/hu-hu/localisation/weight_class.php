<?php
$_['heading_title']			='Súlymértékek';
$_['text_success']			='Siker: A súlymérték frissítése megtörtént!';
$_['text_list']			='Súlymértékek listája';
$_['text_add']			='Súlymérték hozzáadása';
$_['text_edit']			='Súlymérték szerkesztése';
$_['column_title']			='Súlymérték';
$_['column_unit']			='Rövidítés';
$_['column_value']			='Érték';
$_['column_action']			='Művelet';
$_['entry_title']			='Súlymérték neve';
$_['entry_unit']			='Rövidítés';
$_['entry_value']			='Érték';
$_['help_value']			='Set to 1.00000 if this is your default weight.';
$_['error_permission']			='Figyelem: A súlymértékek módosítása az Ön számára nem engedélyezett';
$_['error_title']			='Figyelem: A súlymérték megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_unit']			='Figyelem: A súly rövidítése legalább 1, és legfeljebb 4 karakterből álljon!';
$_['error_default']			='Figyelem: Ezt a súlymértéket nem törölheti, mert jelenleg ezt rendelte hozzá az üzleti alapértelmezett rendelési súlymértékeként!';
$_['error_product']			='Figyelem: Ezt a súlymértéket nem törölheti, mert jelenleg %s termékhez rendelték hozzá!';
?>