<?php
$_['heading_title']			='Nyelv';
$_['text_success']			='Siker: A nyelv frissítése megtörtént!';
$_['text_list']			='Nyelvek listája';
$_['text_add']			='Nyelv hozzáadása';
$_['text_edit']			='Nyelv szerkesztése';
$_['column_name']			='Nyelv neve';
$_['column_code']			='Kód';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['entry_name']			='Nyelv neve';
$_['entry_code']			='Kód';
$_['entry_locale']			='Nyelv';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['help_locale']			='Example: en_US.UTF-8,en_US,en-gb,en_gb,english';
$_['help_status']			='Rejtse el/Mutassa a nyelvválasztóban';
$_['error_permission']			='Figyelmeztetés: A nyelvek módosítása az Ön számára nem engedélyezett!';
$_['error_exists']      = 'Figyelem: Már korábban hozzáadta ezt a nyelvet!';
$_['error_name']			='A nyelv neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_code']			='A nyelvi kód 2 karakterből álljon!';
$_['error_locale']			='Szükség van a nyelvre!';
$_['error_default']			='Figyelem: Ezt a nyelvet nem törölheti, mert jelenleg az áruház alapértelmezett nyelvéhez hozzárendelt!';
$_['error_admin']			='Figyelem: Ez a nyelv nem törölhető, mert jelenleg ez az adminisztráció nyelve!';
$_['error_store']			='Figyelem: Ez a nyelv nem törölhető, mert jelenleg hozzá van rendelve a követhező áruházhoz: %s!';
$_['error_order']			='Figyelem: Ezt a nyelvet nem törölheti, mert jelenleg %s rendeléshez hozzárendelt!';
?>