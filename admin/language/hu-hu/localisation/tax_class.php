<?php
$_['heading_title']			='Adóosztály';
$_['text_success']			='Siker: Az adóosztály módosítása megtörtént!';
$_['text_list']			='Adóosztályok listája';
$_['text_add']			='Adósztály hozzáadása';
$_['text_edit']			='Adósztály szerkesztése';
$_['text_shipping']			='Szállítási cím';
$_['text_payment']			='Számlázási cím';
$_['text_store']			='Webáruház cím';
$_['column_title']			='Adóosztály';
$_['column_action']			='Művelet';
$_['entry_title']			='Adóosztály megnevezése';
$_['entry_description']			='Leírás';
$_['entry_rate']			='Áfakulcs';
$_['entry_based']			='Alapul véve';
$_['entry_geo_zone']			='Földrajzi zóna';
$_['entry_priority']			='Prioritás';
$_['error_permission']			='Figyelem: Az adóosztályok módosítása az Ön számára nem engedélyezett';
$_['error_title']			='Az adóosztály megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_description']			='Az áfakulcs leírásának hosszabbnak kell lennie 3 és rövidebbnek 255 karakternél!';
$_['error_product']			='Figyelem: Ezt az adóosztályt nem törölheti, mert jelenleg %s termékhez rendelték hozzá!';
?>