<?php
$_['heading_title']			='Hosszmértékek';
$_['text_success']			='Siker: Módosította a hosszúmértéket!';
$_['text_list']			='Hosszmértékek listája';
$_['text_add']			='Hosszmérték hozzáadása';
$_['text_edit']			='Hosszmérték szerkesztése';
$_['column_title']			='Hosszmérték';
$_['column_unit']			='Egység';
$_['column_value']			='Érték';
$_['column_action']			='Művelet';
$_['entry_title']			='Hosszmérték neve';
$_['entry_unit']			='Egység';
$_['entry_value']			='Érték';
$_['help_value']			='Állítsa be 1.00000-ra ha ez az alapértelmezett hosszmérték.';
$_['error_permission']			='Figyelem: Nincs engedélye a hosszmérték módosítására!';
$_['error_title']			='Figyelem: A hosszmérték neve 3 és 32 karakter közé kell hogy essen!';
$_['error_unit']			='Figyelem: A hosszmérték egysége 1 és 4 karakter közé kell hogy essen!';
$_['error_default']			='Figyelem: Ez a hosszmérték nem törölhető, mert jelenleg ez az alapértelmezett hosszmérték az áruházban!';
$_['error_product']			='Figyelem: Ez a hosszmérték nem törölhető, mert hozzá van rendelve a követhező termékhez: %s!';
?>