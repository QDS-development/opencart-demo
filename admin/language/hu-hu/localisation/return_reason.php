<?php
$_['heading_title']			='Visszaküldés indoklás';
$_['text_success']			='Siker: Módosítottad a visszaküldés indoklásokat!';
$_['text_list']			='Visszaküldés oka';
$_['text_add']			='Visszaküldés oka létrehozása';
$_['text_edit']			='Visszaküldési ok szerkesztése';
$_['column_name']			='Visszaküldés indoklása';
$_['column_action']			='Művelet';
$_['entry_name']			='Visszaküldés indoklása';
$_['error_permission']			='Figyelmeztetés: Nincs jogosúltságod a visszárú műveletek szerkesztéséhez!';
$_['error_name']			='Visszárú indoklása legalább 3 és legfeljebb 32 karakter lehet!';
$_['error_return']			='Figyelmeztetés: Ezt a visszárú műveletet nem lehet törölni mivel %s visszárú termékekhez van rendelve!';
?>