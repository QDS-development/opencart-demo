<?php
$_['heading_title']			='Vásárlási kiegészítők';
$_['text_success']			='Success: You have modified totals!';
$_['text_list']			='Order Total List';
$_['column_name']			='Kiegészítő neve';
$_['column_status']			='Állapot';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['error_permission']			='Az Összes rendelés módosítása az Ön számára nem engedélyezett!';
?>