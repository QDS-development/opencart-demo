<?php
$_['heading_title']			='Hírcsatornák';
$_['text_success']			='Success: You have modified feeds!';
$_['text_list']			='Feed List';
$_['column_name']			='Termékcsatorna neve';
$_['column_status']			='Állapot';
$_['column_action']			='Művelet';
$_['error_permission']			='Figyelmeztetés: Termékcsatornák módosítása az Ön számára nem engedélyezett!';
?>