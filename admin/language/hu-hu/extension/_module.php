<?php
$_['heading_title']			='Modulok';
$_['text_success']			='Success: You have modified modules!';
$_['text_layout']			='Miután telepítette és beállította a modult a <a href="%s" class="alert-link">Felületek oldalon</a> tudja elhelyezni az oldalon!';
$_['text_add']			='Add Module';
$_['text_list']			='Module List';
$_['column_name']			='Modul neve';
$_['column_action']			='Művelet';
$_['entry_code']			='Modul';
$_['entry_name']			='Modul neve';
$_['error_permission']			='Figyelmeztetés: Modulok módosítása az Ön számára nem engedélyezett!';
$_['error_name']			='Module neve minimum 3 és maximum 64 karakter lehet';
$_['error_code']			='Extension required!';
?>