<?php
$_['heading_title']			='Fizetés';
$_['text_success']			='Success: You have modified payments!';
$_['text_list']			='Payment List';
$_['column_name']			='Fizetési mód';
$_['column_status']			='Állapot';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['error_permission']			='A fizetés módok beállításainak módosítása az Ön számára nem engedélyezett!';
?>