<?php
$_['heading_title']			='Szállítás';
$_['text_success']			='Success: You have modified shipping!';
$_['text_list']			='Shipping List';
$_['column_name']			='Szállítási mód';
$_['column_status']			='Állapot';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['error_permission']			='A Szállítás módosítása az Ön számára nem engedélyezett!';
?>