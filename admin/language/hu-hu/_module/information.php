<?php
$_['heading_title']			='Információk';
$_['text_module']			='Modulok';
$_['text_success']			='Siker: Az információk modul módosítása megtörtént!';
$_['text_edit']			='Edit Information Module';
$_['entry_status']			='Állapot';
$_['error_permission']			='Figyelmeztetés: Az információk modul módosítása az Ön számára nem engedélyezett!';
?>