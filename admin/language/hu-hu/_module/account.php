<?php
$_['heading_title']			='Fiók';
$_['text_module']			='Modulok';
$_['text_success']			='Siker: Fiók módosítása megtörtént!';
$_['text_edit']			='Edit Account Module';
$_['entry_status']			='Állapot';
$_['error_permission']			='Figyelmeztetés: Nincs jogosúltságod a Fiók modul szerkesztéséhez!';
?>