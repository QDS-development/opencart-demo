<?php
$_['heading_title']			='PayPal Express Checkout Button';
$_['text_module']			='Modulok';
$_['text_success']			='Success: You have modified PayPal Express Checkout Button module!';
$_['text_edit']			='Edit PayPal Express Checkout Button Module';
$_['entry_status']			='Állapot';
$_['error_permission']			='Warning: You do not have permission to modify PayPal Express Checkout Button module!';
?>