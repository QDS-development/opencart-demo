<?php
$_['heading_title']			='Slideshow';
$_['text_module']			='Modulok';
$_['text_success']			='Siker: A Slideshow modul módosítása megtörtént!';
$_['text_edit']			='Slideshow modul szerkesztése';
$_['entry_name']			='Modul neve';
$_['entry_banner']			='Banner:';
$_['entry_width']			='Szélesség';
$_['entry_height']			='Magasság';
$_['entry_status']			='Állapot';
$_['error_permission']			='Figyelmeztetés: Nincs jogosúltságod a Slideshow modul szerkesztéséhez!';
$_['error_name']			='Module neve minimum 3 és maximum 64 karakter lehet';
$_['error_width']			='Szélesség kötelező!';
$_['error_height']			='Magasság kötelező!';
?>