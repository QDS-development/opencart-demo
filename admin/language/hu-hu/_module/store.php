<?php
$_['heading_title']			='Bolt';
$_['text_module']			='Modulok';
$_['text_success']			='Siker: A bolt modul módosítása megtörtént!';
$_['text_edit']			='Áruház modul szerkesztése';
$_['entry_admin']			='Admin felhasználók csak:';
$_['entry_status']			='Állapot';
$_['error_permission']			='Figyelmeztetés: Nincs jogosúltságod a Bolt modul szerkesztéséhez!';
?>