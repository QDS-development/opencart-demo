<?php
$_['heading_title']			='Kategória';
$_['text_module']			='Modulok';
$_['text_success']			='Siker: A kategória modul módosítása megtörtént!';
$_['text_edit']			='Kategória modul szerkesztése';
$_['entry_status']			='Állapot';
$_['error_permission']			='Figyelmeztetés: A kategória modul módosítása az Ön számára nem engedélyezett!';
?>