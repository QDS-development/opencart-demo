<?php
$_['heading_title']			='Új jelszó kérése';

$_['text_password']			='Adj meg egy jelszót.';
$_['text_success']			='A jelszavát sikeresen módosította.';
$_['entry_password']			='Jelszó';
$_['entry_confirm']			='Megerősít';
$_['error_password']			='A jelszó legalább 4 és legfeljebb 20 karakter lehet!';
$_['error_confirm']			='A két jelszó nem egyezik!';
?>