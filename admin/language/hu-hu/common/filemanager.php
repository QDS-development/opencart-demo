<?php
$_['heading_title']			='Kép kezelő';
$_['text_uploaded']			='A fájlt sikeresen feltöltötte.';
$_['text_directory']			='A könyvtár sikeresen elkészült';
$_['text_delete']			='A fájl vagy könyvtár törölve lett!';
$_['entry_search']			='Keresés..';
$_['entry_folder']			='Új mappa:';
$_['error_permission']			='Figyelem: Az engedély megtagadva!';
$_['error_filename']			='Figyelem: A filenév 3 és 255 karakter hosszúságú lehet!';
$_['error_folder']			='Warning: Folder name must be a between 3 and 255!';
$_['error_exists']			='Figyelem: Ezen a néven már létezik file vagy könyvtár!';
$_['error_directory']			='Kérem jelöljön ki egy könyvtárat!';
$_['error_filesize']   = 'Figyelem: Helytelen file méret!';
$_['error_filetype']			='Figyelem: Helytelen file típus!';
$_['error_upload']			='Figyelem: A file ismeretlen okok miatt nem tölthető fel!';
$_['error_delete']			='Figyelem: Nem törölheti ezt a könyvtárat!';
?>