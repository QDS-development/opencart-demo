<?php
$_['heading_title']			='Belépés az adminisztrációs felületre';
$_['text_heading']			='Belépés';
$_['text_login']			='Kérjük, adja meg a bejelentkezési adatait';
$_['text_forgotten']			='Elfelejtett jelszó';
$_['entry_username']			='Felhasználónév';
$_['entry_password']			='Jelszó';
$_['button_login']			='Belépés';
$_['error_login']			='Nincs egyező felhasználónév és/vagy jelszó.';
$_['error_token']			='Érvénytelen. Jelentkezzen be újra.';
?>