<?php
$_['heading_title']			='Elfelejtette a jelszavát?';
$_['text_forgotten']			='Elfelejtett jelszó';
$_['text_your_email']			='E-mail címed';
$_['text_email']			='Adja meg a felhasználó nevéhez tartozó e-mail címet. Ezután nyomja meg az elküldés gombot, hogy e-mail címére megkapja a linket.';
$_['text_success']			='A megerősítő link el lett küldve az e-mail címére.';
$_['entry_email']			='E-mail cím';
$_['entry_password']			='Új jelszó:';
$_['entry_confirm']			='Megerősít';
$_['error_email']			='A megadott e-mail cím nem található, próbálja meg újra!';
$_['error_password']			='A jelszó legalább 4 és legfeljebb 20 karakter lehet!';
$_['error_confirm']			='A két jelszó nem egyezik!';
?>