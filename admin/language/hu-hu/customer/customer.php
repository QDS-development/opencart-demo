<?php
$_['heading_title']			='Vásárlók';
$_['text_success']			='Sikeresen módosított vásárlók';
$_['text_list']			='Vásárlók listája';
$_['text_add']			='Vásárló hozzáadása';
$_['text_edit']			='Vásárló szerkesztése';
$_['text_default']			='Alapértelmezett';
$_['text_balance']			='Egyenleg';
$_['column_name']			='Vásárló neve';
$_['column_email']			='E-mail';
$_['column_customer_group']			='Vásárlói csoport';
$_['column_status']			='Állapot';
$_['column_date_added']			='Dátum hozzáadva';
$_['column_comment']			='Komment';
$_['column_description']			='Leírás';
$_['column_amount']			='Összeg';
$_['column_points']			='Pontok';
$_['column_ip']			='IP';
$_['column_total']			='Teljes beszámoló';
$_['column_action']			='Művelet';
$_['entry_customer_group']			='Vásárlói csoport';
$_['entry_firstname']			='Keresztnév';
$_['entry_lastname']			='Vezetéknév';
$_['entry_email']			='E-mail';
$_['entry_telephone']			='Telefonszám';
$_['entry_fax']			='Fax';
$_['entry_newsletter']			='Hírlevél';
$_['entry_status']			='Állapot';
$_['entry_approved']			='Engedélyezett';
$_['entry_safe']			='Megbízható';
$_['entry_password']			='Jelszó';
$_['entry_confirm']			='Jelszó megerősítés';
$_['entry_company']			='Cég';
$_['entry_address_1']			='Címsor 1';
$_['entry_address_2']			='Címsor 2';
$_['entry_city']			='Város';
$_['entry_postcode']			='Irányítószám';
$_['entry_country']			='Ország';
$_['entry_zone']			='Megye';
$_['entry_default']			='Alapértelmezett cím';
$_['entry_comment']			='Komment';
$_['entry_description']			='Leírás';
$_['entry_amount']			='Összeg';
$_['entry_points']			='Pontok';
$_['entry_name']			='Vásárló neve';
$_['entry_ip']			='IP';
$_['entry_date_added']			='Dátum hozzáadva';
$_['help_safe']			='Set to true to avoid this customer from being caught by the anti-fraud system';
$_['help_points']			='Használja a minus-t a pontok eltávolításához!';
$_['error_warning']			='Figyelem: Javítsa ki a hibát az űrlapon!';
$_['error_permission']			='Figyelem: Nincs jogosultsága a vásárló módosításához!';
$_['error_exists']			='Figyelem: Ezt az E-mailcímet már regisztrálták.';
$_['error_firstname']			='Keresztnév minimum 1 és maximum 32 karakter lehet!';
$_['error_lastname']			='Vezetéknév minimum 1 és maximum 32 karakter lehet!';
$_['error_email']			='Az E-mailcím nem tűnik valósnak!';
$_['error_telephone']			='A telefonszám minimum 3 és maximum 32 karakter lehet!';
$_['error_password']			='A jelszó minimum 4 és maximum 20 karakter lehet!';
$_['error_confirm']			='A jelszó és a jelszó megerősítés nem egyezik!';
$_['error_address_1']			='A cím minimum 3 és maximum 128 karakter lehet!';
$_['error_city']			='A város minimum 2 és maximum 128 karakter lehet!';
$_['error_postcode']			='Az irányítószám minimum 2 és maximum 10 karakter lehet ebben az országban!';
$_['error_country']			='Kérjük, válasszon egy országot!';
$_['error_zone']			='Válasszon ki megyét!';
$_['error_zone']            = '%s kötelező!';
?>