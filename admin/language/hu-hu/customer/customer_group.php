<?php
$_['heading_title']			='Vásárlói csoportok';
$_['text_success']			='Sikeresen módosította a vásárlói csoportokat';
$_['text_list']			='Vásárlói csoportok listája';
$_['text_add']			='Vásárlói csoport hozzáadása';
$_['text_edit']			='Vásárlói csoport szerkesztése';
$_['column_name']			='Vásárlói csoport neve';
$_['column_sort_order']			='Sorrend';
$_['column_action']			='Művelet';
$_['entry_name']			='Vásárlói csoport neve';
$_['entry_description']			='Leírás';
$_['entry_approval']			='Új vásárlók jóváhagyása';
$_['entry_sort_order']			='Sorrend';
$_['help_approval']			='A vásárlók csak abban az esetben tudnak belépni a fiókjukba, ha egy adminisztrátor 
jóváhagyta őket!';
$_['error_permission']			='Figyelem: Nincs jogosultsága a vásárlói csoportok szerkesztéséhez!';
$_['error_name']			='A vásárlói csoport neve minimum 3 és maximum 32 karakter lehet!';
$_['error_default']			='Figyelem: Ez a vásárlói csoport mindaddig nem törölhető, amíg a webáruház alapértelmezett vásárlói csoportjának van beállítva!';
$_['error_store']			='Figyelem: Ez a vásárlói csoport mindaddig nem törölhető, amíg a(z) %s webáruházban 
használatban van!';
$_['error_customer']			='Figyelem: This customer group cannot be deleted as it is currently assigned to %s 
customers!';
?>