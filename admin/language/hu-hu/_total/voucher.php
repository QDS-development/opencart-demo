<?php
$_['heading_title']			='Ajándékutalvány';
$_['text_total']			='Összes rendelés';
$_['text_success']			='Siker: Módosítottad az ajándékutalvány összesítését!';
$_['text_edit']			='Edit Gift Voucher Total';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Nincs jogosultsága az ajándékutalványok módosításához!';
?>