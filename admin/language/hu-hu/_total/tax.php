<?php
$_['heading_title']			='Adók';
$_['text_total']			='Összes rendelés';
$_['text_success']			='Siker: Az összes adó módosítása megtörtént!';
$_['text_edit']			='Edit Tax Total';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Figyelmeztetés: Az összes adó módosítása az Ön számára nem engedélyezett!';
?>