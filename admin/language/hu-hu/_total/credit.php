<?php
$_['heading_title']			='Üzlet kredit';
$_['text_total']			='Összes rendelés';
$_['text_success']			='Siker: Módosítottad az üzlet krediteket összesítését!';
$_['text_edit']			='Edit Store Credit Total';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Figyelmeztetés: Nincs jogosultságod az üzlet kreditek összesítésének módosításához!';
?>