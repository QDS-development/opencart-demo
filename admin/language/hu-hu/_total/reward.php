<?php
$_['heading_title']			='Hűségpontjaim';
$_['text_total']			='Összes rendelés';
$_['text_success']			='Sikeresen módosítottad a hűségpontok összesítését!';
$_['text_edit']			='Edit Reward Points Total';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Figyelmeztetés: Nincs jogosultságod a hűségpontok összesítésnek módosításához!';
?>