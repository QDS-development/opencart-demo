<?php
$_['heading_title']			='Kezelési díj';
$_['text_total']			='Összes';
$_['text_success']			='Siker: Módosította az összes kezelési költséget!';
$_['text_edit']			='Edit Handling Fee Total';
$_['entry_total']			='Összes:';
$_['entry_fee']			='Díj:';
$_['entry_tax_class']			='Tax Class';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['help_total']			='The checkout total the order must reach before this order total becomes active.';
$_['error_permission']			='Figyelmeztetés: Nincs engedélye ahhoz, hogy módosítsa a kezelési költséget!';
?>