<?php
$_['heading_title']			='Kupon';
$_['text_total']			='Összes rendelés';
$_['text_success']			='Az ajándékutalvány módosítása megtörtént!';
$_['text_edit']			='Kupon szerkesztése';
$_['entry_status']			='Állapot';
$_['entry_sort_order']			='Sorrend';
$_['error_permission']			='Az ajándékutalvány módosítása az Ön számára nem engedélyezett!';
?>