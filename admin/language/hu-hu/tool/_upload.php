<?php
$_['heading_title']			='Feltöltések';
$_['text_success']			='Success: You have modified uploads!';
$_['text_list']			='Upload List';
$_['column_name']			='Upload Name';
$_['column_filename']			='Fájlnév';
$_['column_date_added']			='Hozzáadás dátuma';
$_['column_action']			='Művelet';
$_['entry_name']			='Upload Name';
$_['entry_filename']			='Fájlnév';
$_['entry_date_added']			='Hozzáadás dátuma';
$_['error_permission']			='Warning: You do not have permission to modify uploads!';
?>