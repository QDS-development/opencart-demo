<?php
$_['heading_title']			='Biztonsági mentés / Visszaállítás';
//$_['text_backup']			='Biztonsági mentés letöltése';
$_['text_success']			='Az adatbázis importálása sikeres!';
//$_['text_list']			='Upload List';
$_['entry_import']     = 'Import';
$_['entry_export']     = 'Export';
$_['error_permission']			='Figyelmeztetés: A biztonsági mentések módosítása az Ön számára nem engedélyezett';
$_['error_export']			='Warning: You must select at least one table to export!';
$_['error_empty']			='A feltöltött fájl üres!';
?>