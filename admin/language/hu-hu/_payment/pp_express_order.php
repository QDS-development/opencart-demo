<?php
$_['text_payment_info']			='Fizetési információk';
$_['text_capture_status']			='Capture status';
$_['text_amount_auth']			='Amount authorised';
$_['text_amount_captured']			='Amount captured';
$_['text_amount_refunded']			='Amount refunded';
$_['text_capture_amount']			='Capture amount';
$_['text_complete_capture']			='Complete capture';
$_['text_transactions']			='Tranzakcióim';
$_['text_complete']			='Complete';
$_['text_confirm_void']			='If you void you cannot capture any further funds';
$_['text_view']			='View';
$_['text_refund']			='Refund';
$_['text_resend']			='Resend';
$_['success_transaction_resent']			='Transaction was successfully resent';
$_['column_trans_id']			='Tranzakció azonosító';
$_['column_amount']			='Összeg';
$_['column_type']			='Payment type';
$_['column_status']			='Állapot';
$_['column_pend_reason']			='Pending reason';
$_['column_date_added']			='Létrehozás dátuma';
$_['column_action']			='Művelet';
$_['button_void']			='Void';
$_['button_capture']			='Capture';
$_['error_capture_amt']			='Enter an amount to capture';
$_['error_timeout']			='Request timed out';
$_['error_transaction_missing']			='Transaction could not be found';
?>