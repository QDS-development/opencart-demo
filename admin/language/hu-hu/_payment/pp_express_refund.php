<?php
$_['heading_title']			='Refund Transaction';
$_['text_pp_express']			='PayPal Express Checkout';
$_['text_current_refunds']			='Refunds have already been done for this transaction. The max refund is';
$_['text_refund']			='Refund';
$_['entry_transaction_id']			='Tranzakció azonosító';
$_['entry_full_refund']			='Full refund';
$_['entry_amount']			='Összeg';
$_['entry_message']			='Üzenet';
$_['button_refund']			='Issue refund';
$_['error_partial_amt']			='You must enter a partial refund amount';
$_['error_data']			='Data missing from request';
?>